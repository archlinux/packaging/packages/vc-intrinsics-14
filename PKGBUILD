# Maintainer: Torsten Keßler <tpkessler@archlinux.org>

_pkgname=vc-intrinsics
pkgname=$_pkgname-14
pkgver=0.13.0.r2+g910db48
pkgrel=1
pkgdesc="Intrinsics on top of LLVM representing SIMD semantics for GPU programs (LLVM 14)"
arch=(x86_64)
url="https://github.com/intel/vc-intrinsics"
license=(MIT)
makedepends=(cmake python llvm14 git)
# From IGC release notes or latest release
_commit=910db4801d4a029834606e3e42a8d60358e74fdf
source=(git+${url}.git#commit=$_commit)
sha256sums=('SKIP')

pkgver() {
  cd ${_pkgname}
  git describe --tags | sed 's/^v//;s/\([^-]*-g\)/r\1/;s/-/./;s/-/+/'
}

prepare() {
  cd ${_pkgname}
  git revert -n bda6db9b23d695998faac74ace4094adb285ca28
}

build() {
  export CXXFLAGS+=" -ffat-lto-objects"
  cmake -B build -S ${_pkgname} \
    -G 'Unix Makefiles' \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr/lib/llvm14 \
    -DLLVM_DIR=/usr/lib/llvm14/lib/cmake/llvm \
    -Wno-dev
  cmake --build build
}

package() {
  DESTDIR="${pkgdir}" cmake --install build
}
